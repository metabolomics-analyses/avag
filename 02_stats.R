options(java.parameters = "-Xmx2g" )
library(xlsx)
library(lme4)
library(multcomp)
library(fdrtool)
library(multtest)
library(parallel)
library(pcaMethods)
library(chemhelper)
library(massageR)



# Get the data only from peaklist
data <- peaklist[,   9:(length(peaklist)-3)    ]
data <- t(data)
colnames(data) <- paste0("var",1:ncol(data))

varlist <- colnames(data)

data <- cbind.data.frame(as.data.frame(factors), data)

# remove QCs
data <- data[!is.na(data$person),]

data[,c(1:length(factors))] <- droplevels(data[,c(1:length(factors))])



# Possible normalizations -------------------------------------------------
## batch correction # doesn't seem relevant for urine
# Used for plasma only
data[,-c(1:length(factors))] <- group_correct(data[,-c(1:length(factors))],data$batch,zero.is.na=T)



## Normalize by (2E)-3-(2H5)Phenylacrylic acid
# NOT used
# int_std_idx <- which.min(abs(peaklist$mz-c(154.0911,152.0765)[settings$general$ionmode]))
# peaklist[int_std_idx,c("mz","rt")]
# 
# int_std_matrix <- matrix(data[,int_std_idx+length(factors)]    ,byrow=F,ncol=ncol(data)-length(factors),nrow=nrow(data))
# data[,-c(1:length(factors))] <- data[,-c(1:length(factors))] / int_std_matrix




## Normalize by sum of features (for urine)
# NOT used
# data[,-c(1:length(factors))] <- apply(   data[,-c(1:length(factors))]   ,1,   function(x) x/sum(x)    )



## Urine volume difference
# NOT used
# boxplot(urine ~ class,data=data)
# plot(data$urine,  apply(data[,-c(1:length(factors))],1,sum)   )
# 
# sample_sum <- apply(data[,-c(1:length(factors))],1,sum)
# boxplot(sample_sum ~ data$class)



## normalize by creatinine
# used for urine
# for(i in 1:nrow(data)){
#   data[i,-c(1:length(factors))] <-   data[i,-c(1:length(factors))] / data$urine[i]
# }
# 
# 
# data <- data[!is.na(data$urine),]




## PCA #####################################
settings$pcs = c(1,2)


color_by <- "class"
color <- as.numeric(data[,color_by])
names(color) <- as.character(data[,color_by])




pca <- pca(data[,-c(1:length(factors))],scale="uv",method="nipals",nPcs=max(settings$pcs))
#pca=pca(data_area,scale="uv",method="svd",nPcs=5)



# Set up plot
loading_scale=    quantile(abs(scores(pca)[,settings$pcs]),0.90)   /  quantile(abs(loadings(pca)[,settings$pcs]),0.90)    

xlab=paste("PC",settings$pcs[1]," (",signif(pca@R2[settings$pcs[1]]*100,3),"%)",sep="")
ylab=paste("PC",settings$pcs[2]," (",signif(pca@R2[settings$pcs[2]]*100,3),"%)",sep="")
xlim=c(    floor(min(     c(   scores(pca)[,settings$pcs[1]]  ,loadings(pca)[,settings$pcs[1]]*loading_scale   )         ))   ,   ceiling(max(c(    scores(pca)[,settings$pcs[1]]  ,loadings(pca)[,settings$pcs[1]]*loading_scale  )   )) )
ylim=c(    floor(min(     c(   scores(pca)[,settings$pcs[2]]  ,loadings(pca)[,settings$pcs[2]]*loading_scale   )         ))   ,   ceiling(max(c(    scores(pca)[,settings$pcs[2]]  ,loadings(pca)[,settings$pcs[2]]*loading_scale  )   )) )
res_scale=600/72


# now do the plot
plot(loadings(pca)[,settings$pcs[1]]*loading_scale,loadings(pca)[,settings$pcs[2]]*loading_scale,pch=20,xlab=xlab,ylab=ylab,ylim=ylim,xlim=xlim,col="grey",main="some title")
arrows(0,0,scores(pca)[,settings$pcs[1]],scores(pca)[,settings$pcs[2]],xlab="PC1",ylab="PC2",lty=2,lwd=2,length=0,col=color)
text(scores(pca)[,settings$pcs[1]],scores(pca)[,settings$pcs[2]],labels=data$class,xlab="PC1",ylab="PC2",cex=1,col=color)
legend("bottomright",legend=unique(names(color)),lty=c(1,1),col=unique(color),cex=0.5,lwd=1,pch=20,pt.cex=1,y.intersp=1.1,title=color_by)








# lets test gender:arm ###########################
factor1_check_func <- function(x) {
  require(lme4)
  
  
  formula1 <- substitute(var ~ class 
                              + gender
                              + arm
                              + (1|person) 
                              + (1|batch)
                        
                              + (1|class:gender) 
                              + (1|class:arm) 
                              + (1|gender:arm) # test this
                        
                        , list(var = as.name(x)))
  
  
  
  formula2 <- substitute(var ~ class 
                         + gender
                         + arm
                         + (1|person)
                         + (1|batch)
                         
                         + (1|class:gender) 
                         + (1|class:arm) 
                         
                         
                         , list(var = as.name(x)))
  
  
  if(all(data[,x]==1)) return(NA)
  
  fm1 <- lmer(formula = formula1, data = data, REML=FALSE)
  fm2 <- lmer(formula = formula2, data = data, REML=FALSE)
  
  anova <- anova(fm1,fm2)
  p <- anova[2,"Pr(>Chisq)"]
  return(p)
  
}




cl=makeCluster(rep("localhost",as.numeric(parallel:::detectCores()-1)))
clusterExport(cl,c('data'))
factor1_check=parSapply(cl,varlist,factor1_check_func)
stopCluster(cl)


min(factor1_check)
hist(factor1_check,breaks=seq(0,1,0.05))


isNotNA <- which(!is.na(factor1_check))
temp <- fdrtool(factor1_check[isNotNA],statistic = "pvalue")
temp <- temp$qval
factor1_check_q <- rep(NA,length(factor1_check))
factor1_check_q[isNotNA] <- temp



min(factor1_check_q,na.rm=T)
hist(factor1_check_q,breaks=seq(0,1,0.05))










# lets test class:arm ###########################
factor2_check_func <- function(x) {
  require(lme4)
  
  
  formula1 <- substitute(var ~ class 
                         + gender
                         + arm
                         + (1|person)
                         + (1|batch)
                         
                         + (1|class:gender) 
                         + (1|class:arm)  # test this
                         
                         , list(var = as.name(x)))
  
  
  
  formula2 <- substitute(var ~ class 
                         + gender
                         + arm
                         + (1|person)
                         + (1|batch)
                         
                         + (1|class:gender) 

                         
                         , list(var = as.name(x)))
  
  if(all(data[,x]==1)) return(NA)
  fm1 <- lmer(formula = formula1, data = data, REML=FALSE)
  fm2 <- lmer(formula = formula2, data = data, REML=FALSE)
  
  anova <- anova(fm1,fm2)
  p <- anova[2,"Pr(>Chisq)"]
  return(p)
  
}



cl=makeCluster(rep("localhost",as.numeric(parallel:::detectCores()-1)))
clusterExport(cl,c('data'))
factor2_check=parSapply(cl,varlist,factor2_check_func)
stopCluster(cl)


min(factor2_check,na.rm=T)
hist(factor2_check,breaks=seq(0,1,0.05))



isNotNA <- which(!is.na(factor2_check))
temp <- fdrtool(factor2_check[isNotNA],statistic = "pvalue")
temp <- temp$qval
factor2_check_q <- rep(NA,length(factor2_check))
factor2_check_q[isNotNA] <- temp




min(factor2_check_q,na.rm=T)
hist(factor2_check_q,breaks=seq(0,1,0.05))






# lets test class:gender ###########################
factor3_check_func <- function(x) {
  require(lme4)
  
  
  formula1 <- substitute(var ~ class 
                         + gender
                         + arm
                         + (1|person)
                         + (1|batch)
                         
                         + (1|class:gender)   # test this
                         
                         , list(var = as.name(x)))
  
  
  
  formula2 <- substitute(var ~ class 
                         + gender
                         + arm
                         + (1|person)
                         + (1|batch)

                         
                         , list(var = as.name(x)))
  
  if(all(data[,x]==1)) return(NA)
  fm1 <- lmer(formula = formula1, data = data, REML=FALSE)
  fm2 <- lmer(formula = formula2, data = data, REML=FALSE)
  
  anova <- anova(fm1,fm2)
  p <- anova[2,"Pr(>Chisq)"]
  return(p)
  
}




cl=makeCluster(rep("localhost",as.numeric(parallel:::detectCores()-1)))
clusterExport(cl,c('data'))
factor3_check=parSapply(cl,varlist,factor3_check_func)
stopCluster(cl)


min(factor3_check,na.rm=T)
hist(factor3_check,breaks=seq(0,1,0.05))




isNotNA <- which(!is.na(factor3_check))
temp <- fdrtool(factor3_check[isNotNA],statistic = "pvalue")
temp <- temp$qval
factor3_check_q <- rep(NA,length(factor3_check))
factor3_check_q[isNotNA] <- temp




min(factor3_check_q,na.rm=T)
hist(factor3_check_q,breaks=seq(0,1,0.05))







# lets test arm ###########################
factor4_check_func <- function(x) {
  require(lme4)
  
  
  formula1 <- substitute(var ~ class 
                         + gender
                         + arm
                         + (1|person)
                         + (1|batch)
                         
                         , list(var = as.name(x)))
  
  
  
  formula2 <- substitute(var ~ class 
                         + gender
                         
                         + (1|person)
                         + (1|batch)

                         , list(var = as.name(x)))
  
  
  if(all(data[,x]==1)) return(NA)
  fm1 <- lmer(formula = formula1, data = data, REML=FALSE)
  fm2 <- lmer(formula = formula2, data = data, REML=FALSE)
  
  
  #summary(aov(CDCA ~ class + Error(person/class)  ,data=data))
  
  
  anova <- anova(fm1,fm2)
  p <- anova[2,"Pr(>Chisq)"]
  return(p)
  
}





cl=makeCluster(rep("localhost",as.numeric(parallel:::detectCores()-1)))
clusterExport(cl,c('data'))
factor4_check=parSapply(cl,varlist,factor4_check_func)
stopCluster(cl)


min(factor4_check,na.rm=T)
hist(factor4_check,breaks=seq(0,1,0.05))



isNotNA <- which(!is.na(factor4_check))
temp <- fdrtool(factor4_check[isNotNA],statistic = "pvalue")
temp <- temp$qval
factor4_check_q <- rep(NA,length(factor4_check))
factor4_check_q[isNotNA] <- temp




min(factor4_check_q,na.rm=T)
hist(factor4_check_q,breaks=seq(0,1,0.05))











# lets test gender ###########################
factor5_check_func <- function(x) {
  require(lme4)
  
  
  formula1 <- substitute(var ~ class 
                         + gender
                         + arm
                         + (1|person)
                         + (1|batch)
                         
                         , list(var = as.name(x)))
  
  
  
  formula2 <- substitute(var ~ class 
                         
                         + arm
                         + (1|person)
                         + (1|batch)

                         
                         , list(var = as.name(x)))
  
  
  if(all(data[,x]==1)) return(NA)
  fm1 <- lmer(formula = formula1, data = data, REML=FALSE)
  fm2 <- lmer(formula = formula2, data = data, REML=FALSE)
  
  anova <- anova(fm1,fm2)
  p <- anova[2,"Pr(>Chisq)"]
  return(p)
  
}






cl=makeCluster(rep("localhost",as.numeric(parallel:::detectCores()-1)))
clusterExport(cl,c('data'))
factor5_check=parSapply(cl,varlist,factor5_check_func)
stopCluster(cl)


min(factor5_check,na.rm=T)
hist(factor5_check,breaks=seq(0,1,0.05))




isNotNA <- which(!is.na(factor5_check))
temp <- fdrtool(factor5_check[isNotNA],statistic = "pvalue")
temp <- temp$qval
factor5_check_q <- rep(NA,length(factor5_check))
factor5_check_q[isNotNA] <- temp




min(factor5_check_q,na.rm=T)
sum(factor5_check_q<0.05,na.rm=T)
hist(factor5_check_q,breaks=seq(0,1,0.05))










# lets test person ###########################

factor6_check_func <- function(x) {
  require(lme4)
    
  formula1 <- substitute(var ~ class 
                         + gender
                         + arm
                         + (1|person)
                         + (1|batch)
                         
                         , list(var = as.name(x)))
  
  
  
  formula2 <- substitute(var ~ class 
                         + gender
                         + arm
                         + (1|batch)
                         
                         
                         , list(var = as.name(x)))
  
  
  if(all(data[,x]==1)) return(NA)
  fm1 <- lmer(formula = formula1, data = data, REML=FALSE)
  fm2 <- lmer(formula = formula2, data = data, REML=FALSE)
  
  anova <- anova(fm1,fm2)
  p <- anova[2,"Pr(>Chisq)"]
  return(p)
  
  
  
  
  
  
  # method 1 for comparing a model with and without random factors
#   fm1 <- lmer(formula = formula1, data = data, REML=FALSE)
#   fm2 <- lm(formula = formula2, data = data)
#   
#   NL2 <- -logLik(fm1)
#   NL1 <- -logLik(fm2)
#   devdiff <- 2*(NL1-NL2)
#   dfdiff <- attr(NL2,"df")-attr(NL1,"df")
#   p1 <- as.numeric(pchisq(devdiff,dfdiff,lower.tail=FALSE))
  
  
  
  # method 2. Gives (practically) same as method 1 but returns some zero values when method 1 returns small p values
#   f   <<- formula(paste(x, "class + gender + arm", sep = "~")) # I have no idea why this <<- is needed!
#   fm3 <- lme(fixed=f,random= ~1|person, data = data)
#   
#   fm4 <- lm(formula = formula2, data = data)
#   
#   anova <- anova(fm3,fm4)
#   p2 <- anova[2,"p-value"]
#   rm(f)
  
  
  return(p1)
  
}





cl=makeCluster(rep("localhost",as.numeric(parallel:::detectCores()-1)))
clusterExport(cl,c('data'))
factor6_check=parSapply(cl,varlist,factor6_check_func)
stopCluster(cl)


min(factor6_check,na.rm=T)
hist(factor6_check,breaks=seq(0,1,0.05))



isNotNA <- which(!is.na(factor6_check))
temp <- fdrtool(factor6_check[isNotNA],statistic = "pvalue")
temp <- temp$qval
factor6_check_q <- rep(NA,length(factor6_check))
factor6_check_q[isNotNA] <- temp




min(factor6_check_q,na.rm=T)
hist(factor6_check_q,breaks=seq(0,1,0.05))
















# lets test batch ###########################

factor7_check_func <- function(x) {
  require(lme4)
  
  formula1 <- substitute(var ~ class 
                         + gender
                         + arm
                         + (1|person)
                         + (1|batch)
                         
                         , list(var = as.name(x)))
  
  
  
  formula2 <- substitute(var ~ class 
                         + gender
                         + arm
                         + (1|person)
                         
                         
                         , list(var = as.name(x)))
  
  
  if(all(data[,x]==1)) return(NA)
  fm1 <- lmer(formula = formula1, data = data, REML=FALSE)
  fm2 <- lmer(formula = formula2, data = data, REML=FALSE)
  
  anova <- anova(fm1,fm2)
  p <- anova[2,"Pr(>Chisq)"]
  return(p)
  
}





cl=makeCluster(rep("localhost",as.numeric(parallel:::detectCores()-1)))
clusterExport(cl,c('data'))
factor7_check=parSapply(cl,varlist,factor7_check_func)
stopCluster(cl)


min(factor7_check,na.rm=T)
hist(factor7_check,breaks=seq(0,1,0.05))



isNotNA <- which(!is.na(factor7_check))
temp <- fdrtool(factor7_check[isNotNA],statistic = "pvalue")
temp <- temp$qval
factor7_check_q <- rep(NA,length(factor7_check))
factor7_check_q[isNotNA] <- temp




min(factor7_check_q,na.rm=T)
sum(factor7_check_q<0.05,na.rm=T)
hist(factor7_check_q,breaks=seq(0,1,0.05))
























# lets do the real model ###########################

# overall model
treatment_effect_func <- function(x) {
  require(lme4)
  
  formula1 <- substitute(var ~ class 
                             + gender
                             + arm
                             + (1|person)
                             #+ (1|batch)
                         
                         , list(var = as.name(x)))
  
  
  
  formula2 <- substitute(var ~ gender
                             + arm
                             + (1|person) 
                             #+ (1|batch)
                         
                         , list(var = as.name(x)))
  
  
#    summary(aov(CA1 ~ class + Error(person),data=data))
#   
#   require(nlme)
#   anova(lme(CA1 ~ class, random=~1 | person, method="REML", data=data))
#   
  
if(all(data[,x]==1)) return(NA)
  fm1 <- lmer(formula = formula1, data = data, REML=FALSE)
  fm2 <- lmer(formula = formula2, data = data, REML=FALSE)
  
  anova <- anova(fm1,fm2)
  p <- anova[2,"Pr(>Chisq)"]
  return(p)
  
}




cl=makeCluster(rep("localhost",as.numeric(parallel:::detectCores()-1)))
clusterExport(cl,c('data'))
treatment_effect=parSapply(cl,varlist,treatment_effect_func)
stopCluster(cl)


min(treatment_effect,na.rm=T)
hist(as.vector(treatment_effect),breaks=seq(0,1,0.05))



isNotNA <- which(!is.na(treatment_effect))
temp <- fdrtool(treatment_effect[isNotNA],statistic = "pvalue")
temp <- temp$qval
treatment_effect_fdr <- rep(NA,length(treatment_effect))
treatment_effect_fdr[isNotNA] <- temp



procs="TSBH"
res <-mt.rawp2adjp(treatment_effect, procs)    
treatment_effect_adj =  res$adjp[order(res$index), 2]
rm(res,procs)

sum(treatment_effect_adj[!is.na(treatment_effect_adj)]<0.1,na.rm=T)
sum(treatment_effect_adj[!is.na(treatment_effect_adj)]<0.05,na.rm=T)

sum(treatment_effect_fdr[!is.na(treatment_effect_fdr)]<0.05,na.rm=T)









# Multiple comparision
treatment_effect_mult_func <- function(x) {
  require(lme4)
  require(multcomp)
  formula1 <- substitute(var ~ class 
                         + gender
                         + arm
                         + (1|person)
                         #+ (1|batch)
                         
                         , list(var = as.name(x)))
  
  
  
  if(all(data[,x]==1)){
  temp <- rep(NA,  nrow(expand.grid(levels(data$class),2)))
  names(temp) <- c("Baseline - Apple" ,"Juice - Apple"  ,  "Juice - Baseline")
  return(temp)
  }
  
  fm1 <- lmer(formula = formula1, data = data, REML=FALSE)
  anova <- anova(fm1)
  
  comps <- glht(fm1,linfct = mcp( class = "Tukey"))
  multcomp_summary <- summary(comps,test=adjusted("none"))
  
  # get p-values
  multcomp_p  <-   multcomp_summary$test$pvalues
  
  return(multcomp_p)
}








cl=makeCluster(rep("localhost",as.numeric(parallel:::detectCores()-1)))
clusterExport(cl,c('data'))
treatment_effect_mult <- parSapply(cl,varlist,treatment_effect_mult_func)
stopCluster(cl)



#treatment_effect_mult <- do.call(rbind,treatment_effect_mult)
treatment_effect_mult <- t(treatment_effect_mult)
treatment_effect_mult_vec <- as.vector(treatment_effect_mult)
isNotNA <- which(!is.na(treatment_effect_mult_vec ))
temp <- fdrtool(treatment_effect_mult_vec [isNotNA],statistic = "pvalue")
temp <- temp$qval
treatment_effect_mult_fdr<- rep(NA,length(treatment_effect_mult_vec ))
treatment_effect_mult_fdr[isNotNA] <- temp



dim(treatment_effect_mult_fdr) <- dim(treatment_effect_mult)

colnames(treatment_effect_mult_fdr) <- colnames(treatment_effect_mult)


sig_feature <- apply(treatment_effect_mult_fdr,1,function(x) any(x<0.05))
sum(sig_feature,na.rm=T)

# The minimum q-value for each feature
multcomp_p_adj_min <- apply(treatment_effect_mult_fdr,1,min,na.rm=T)








## write final table ####################
out_table <- data.frame(peaklist,treatment_effect_fdr,treatment_effect_mult)
write.csv(out_table,file=paste(getwd(),'/',settings$general$date,'/peaklist_stats_',c('pos','neg')[settings$general$ionmode],'.csv',sep=""))
  




## Save everything ###################################
save.image(file=paste(getwd(),'/',settings$general$date,'/all_stats_',c('pos','neg')[settings$general$ionmode],'.RData',sep=""))

  




## Boxplots ###############################
require(ggplot2)

data_no_factor <- data[,-(1:length(factors))]


## Stupid boxplot
to_plot <- which(         treatment_effect_fdr[!is.na(treatment_effect_fdr)]<0.1           )
dir.create(paste0(getwd(),'/',settings$general$date,"/plots/boxplots/",c('pos','neg')[settings$general$ionmode]),showWarnings = F, recursive = T)

for(i in to_plot){
  
  plot_fac <- as.character(data$class)
  
  df = data.frame(factor=plot_fac,    data=data_no_factor[,i]  ,  stringsAsFactors=F)
  
  p <- ggplot(df, aes(x=factor, y=data, fill=factor))
  p <- p + geom_boxplot() +
    scale_fill_manual(values = 1:length(plot_fac)  )
  
  p <- p + ggtitle(    paste0(
    "Feature #: ",i,"\n",
    "pcgroup: ",peaklist[i,"pcgroup"],"\n",
    "q-value: ",signif(treatment_effect_fdr[i],2),"\n",
    "mz: ",round(peaklist[i,"mz"],4),"\n",
    "rt: ",round(peaklist[i,"rt"]/60,2),"\n",
    "isotope: ",peaklist[i,"isotopes"],"\n",
    "adduct: ",peaklist[i,"adduct"]
  )     
  )
  
  ggsave(filename=paste0(paste0(getwd(),'/',settings$general$date,"/plots/boxplots/",c('pos','neg')[settings$general$ionmode]),"/order_",sprintf("%05d", i),".png"),plot=p,dpi=150)
  
}

