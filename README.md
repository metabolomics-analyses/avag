# AVAG

This is the data analysis for the AVAG study published at: XXXX
The script is not the cleanest I have done but should be reproduceable.

**Notes:**

* The script runs one mode at a time (see `settings$general$ionmode`) and one sample type (plasma/urine) at a time (see `lpath`).
* Normalization needs to be switched correctly in the script:
  * Plasma uses only batch correction
  * Urine uses Creatinine normalization but no batch correction
* raw data should be gotten from MetaboLights at: https://www.ebi.ac.uk/metabolights/MTBLS469
* A more detailed set of meta-data of the study is also made available in the Dash-in Web based federated analyses website (http://www.enpadasi.eu/deliverables_final_pdf/D3.2.1.pdf)
* Metadata in Dashin are compliant with the need to be extensively annotated in human- and machine-readable form https://dashin.eu/interventionstudies/study/show/38790886
